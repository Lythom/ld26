package ;

/**
 * ...
 * @author Samuel Bouchet
 */

class Main2
{
	static private var ctx:haxe.remoting.Context;
	function new() { }
	
	function foo(x, y) {
		return x + y;
	}

	static function main() {
		ctx = new haxe.remoting.Context();
		ctx.addObject("Server",new Main2());
		if( haxe.remoting.HttpConnection.handleRequest(ctx) )
			return;
		// handle normal request
		neko.Lib.print("This is a remoting server !");
	}
	
}