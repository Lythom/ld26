import com.gigglingcorpse.utils.Profiler;
import com.haxepunk.Engine;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.CheckBox;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.RadioButton;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.gui.Control;
import world.WelcomeWorld;

class Main extends Engine
{
	private var profiler:Profiler;

	public static inline var kScreenWidth:Int = 960;
	public static inline var kScreenHeight:Int = 640;
	public static inline var kFrameRate:Int = 60;
	public static inline var kClearColor:Int = 0xDDDDDD;
	public static inline var kProjectName:String = "HaxePunk";

	public function new()
	{
		super(kScreenWidth, kScreenHeight, kFrameRate, true);
	}

	override public function init()
	{
		//HXP.console.enable();
		//HXP.console.toggleKey = Key.P;
#if debug
	#if flash
		if (flash.system.Capabilities.isDebugger)
	#end
		{
			HXP.console.enable();
			HXP.console.toggleKey = Key.P;
		}
#end
		
		initHaxepunkGui();
		
		HXP.screen.color = kClearColor;
		HXP.screen.scale = 4;
		HXP.scene = new WelcomeWorld();
		
		Input.define("up", [Key.Z, Key.W, Key.UP]);
		Input.define("right", [Key.D, Key.RIGHT]);
		Input.define("down", [Key.S, Key.DOWN]);
		Input.define("left", [Key.Q, Key.A, Key.LEFT]);
		Input.define("action1", [Key.E, Key.ENTER]);
		
		profiler = null;
	}
	public static function main()
	{
		
		new Main();
	}
	
	override public function update():Dynamic 
	{
		if (Input.pressed(Key.R)) {
			if (profiler == null) {
				profiler = new Profiler();
				HXP.stage.addChild(profiler);
				profiler.visible = true;
			} else {
				HXP.stage.removeChild(profiler);
				profiler = null;
			}
		}
		super.update();
	}
	
		
	private function initHaxepunkGui():Void 
	{
		// Choose custom skin. Parameter can be a String to resource or a bitmapData.
		Control.useSkin("gfx/ui/greyDefault.png");
		// The default layer where every component will be displayed on.
		// Most components use severals layers (at least 1 per component child). A child component layer will be <100.
		Control.defaultLayer = 100;
		// Use this to fit your button skin's borders, set the default padding of every new Button and ToggleButton.
		// padding attribute can be changed on instances after creation.
		Button.defaultPadding = 4;
		// Size in px of the tickBox for CheckBoxes. Default is skin native size : 12.
		CheckBox.defaultBoxSize = 12;
		// Same for RadioButtons.
		RadioButton.defaultBoxSize = 12;
		// Label defaults parameters affect every components that uses labels : Button, ToggleButton, CheckBox, RadioButton, MenuItem, Window Title.
		// Those labels are always accessible using "myComponent.label" and you can change specific Labels apperence any time.
		// Label default font (must be a nme.text.Font object).
		Label.defaultFont = nme.Assets.getFont("font/lythgame.ttf");
		// Label defaultColor. Tip inFlashDevelop : use ctrl + shift + k to pick a color.
		Label.defaultColor = 0x1E4E82;
		// Label default Size.
		Label.defaultSize = 8;
	}

}