package plateformer;
import com.haxepunk.HXP;
import com.haxepunk.plateformer.BasicPhysicsEntity;
import nme.geom.Point;

/**
 * ...
 * @author Samuel Bouchet
 */
class Admirator extends BasicPhysicsEntity
{
	private var cd:Float;
	private var gravity:Point;

	public function new(x:Int, y:Int) 
	{
		cd = Math.random() * 100 + 20;
		gravity = new Point(0, (9.81) * 20);
		
		super(x, y);	
		this.forces.push(gravity);
	}
	
	override public function update():Void 
	{
		this.acceleration.normalize(0);
		
		if (cd < 0) {
			this.acceleration.y = -50/HXP.elapsed;
			cd = Math.random() * 100 + 20;
		}
		cd--;
		
		
		super.update();
	}
	
}