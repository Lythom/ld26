package plateformer;
import com.haxepunk.Entity;

/**
 * ...
 * @author Samuel Bouchet
 */
class Guard extends Entity
{
	private var _relativeX:Int;
	private var _followHero:Bool;
	private var _hero:ControlledHero;

	public function new(x:Int, y:Int) 
	{
		super(x, y);
	}
	
	override public function update():Void 
	{
		if (followHero) {
			this.x = this.x + ((_hero.x + relativeX)-this.x)/15;
		}
		super.update();
	}
	
	private function get_relativeX():Int 
	{
		return _relativeX;
	}
	
	private function set_relativeX(value:Int):Int 
	{
		return _relativeX = value;
	}
	
	public var relativeX(get_relativeX, set_relativeX):Int;
	
	private function get_followHero():Bool 
	{
		return _followHero;
	}
	
	private function set_followHero(value:Bool):Bool 
	{
		return _followHero = value;
	}
	
	public var followHero(get_followHero, set_followHero):Bool;
	
	private function get_hero():ControlledHero 
	{
		return _hero;
	}
	
	private function set_hero(value:ControlledHero):ControlledHero 
	{
		return _hero = value;
	}
	
	public var hero(get_hero, set_hero):ControlledHero;
	
}