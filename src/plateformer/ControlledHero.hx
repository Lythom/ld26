package plateformer;
import com.haxepunk.graphics.Image;
import com.haxepunk.HXP;
import com.haxepunk.math.Vector;
import com.haxepunk.normalmap.graphic.NormalMappedImage;
import com.haxepunk.normalmap.NormalMappedBitmapData;
import com.haxepunk.plateformer.BasicPhysicsEntity;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import nme.display.BitmapData;
import nme.geom.Point;
import nme.geom.Vector3D;
import world.GameWorld;

/**
 * ...
 * @author Lythom
 */
class ControlledHero extends BasicPhysicsEntity
{
	private var facteur:Float;
	private var gravity:Point;
	private var graphic1:NormalMappedImage;
	public var canMoveLeft:Bool;
	public var canMove:Bool;

	public function new(x:Int=0, y:Int=0) 
	{
		super(x,y);
		graphic1 = new NormalMappedImage(new NormalMappedBitmapData(HXP.getBitmap("gfx/hero.png"), HXP.getBitmap("gfx/hero_normal.png")));
		
		set_graphic(graphic1);
		graphic1.x = 0;
		graphic1.y = 0;
		setHitbox(graphic1.width - 1, graphic1.height - 1);
		canMoveLeft = true;
		canMove = true;
		
		maxVelocity.x = 200;
		
		// 20 px = 1m
		gravity = new Point(0, (9.81)*20);
	}
	
	public function equipNormal() {
		graphic1 = new NormalMappedImage(new NormalMappedBitmapData(HXP.getBitmap("gfx/hero.png"), HXP.getBitmap("gfx/hero_normal.png")));
		y += 16;
		set_graphic(graphic1);
		setHitbox(graphic1.width - 1, graphic1.height - 1);
	}
	
	public function equipStuff() {
		graphic1 = new NormalMappedImage(new NormalMappedBitmapData(HXP.getBitmap("gfx/hero2.png"), HXP.getBitmap("gfx/hero2_normal.png")));
		y -= 16;
		set_graphic(graphic1);
		setHitbox(graphic1.width - 1, graphic1.height - 1);
	}
	
	public function breakStuff() {
		graphic1 = new NormalMappedImage(new NormalMappedBitmapData(HXP.getBitmap("gfx/hero3.png"), HXP.getBitmap("gfx/hero3_normal.png")));
		set_graphic(graphic1);
		setHitbox(graphic1.width - 1, graphic1.height - 1);
	}
	
	public function equipNewSword() {
		graphic1 = new NormalMappedImage(new NormalMappedBitmapData(HXP.getBitmap("gfx/hero4.png"), HXP.getBitmap("gfx/hero4_normal.png")));
		set_graphic(graphic1);
		setHitbox(graphic1.width - 1, graphic1.height - 1);
	}
	
	override public function added():Void 
	{
		super.added();
		this.forces.push(gravity);
	}
	
	override public function update():Void 
	{
		if(canMove){
			this.acceleration.normalize(0);
			var xAcc:Float = 350;
			var yAcc:Float = 350;
		
			maxVelocity.x = 75;
			
			if (Input.check("up") && onGround)
			{
				this.acceleration.y = -100/HXP.elapsed;
			}
			
			if (Input.check("left") && canMoveLeft)
			{
				this.acceleration.x = - xAcc;
				friction.x = 0;
			} else if (Input.check("right")) {
				this.acceleration.x = xAcc;
				friction.x = 0;
			} else {
				this.acceleration.x = 0;
				friction.x = 0.3;
			}
			
			if (this.y > 480) {
				this.x = 100;
				this.y = 0;
			}
		
			super.update();
		}
	}
}