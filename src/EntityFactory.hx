package ;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.HXP;
import com.haxepunk.normalmap.graphic.NormalMappedImage;
import com.haxepunk.normalmap.NormalMappedBitmapData;
import com.haxepunk.plateformer.BasicPhysicsEntity;
import com.haxepunk.tmx.TmxObject;
import plateformer.Admirator;
import plateformer.ControlledHero;
import plateformer.Guard;

/**
 * ...
 * @author Samuel Bouchet
 */
class EntityFactory
{

	public function new() 
	{
		
	}
	
	static public function hero(obj:TmxObject):Entity
	{
		var e = new ControlledHero(obj.x, obj.y);
		return e;
	}
	
	static public function stuff(obj:TmxObject):Entity
	{
		return(genericEntity(obj));
	}
	
	static public function firstGuard(obj:TmxObject):Entity
	{
		obj.y += 3;
		return(genericEntity(obj));
		
	}
	
	static public function dragonSign(obj:TmxObject):Entity
	{
		return(genericEntity(obj));
	}
	
	static public function dragon(obj:TmxObject):Entity
	{
		return(genericEntity(obj));
	}
	
	static public function sword(obj:TmxObject):Entity
	{
		return(genericEntity(obj));
	}
	
	static public function admirator(obj:TmxObject):Entity
	{
		obj.y -= 3;
		var e = new Admirator(obj.x, obj.y);
		var g = new NormalMappedImage(
			new NormalMappedBitmapData(HXP.getBitmap("gfx/"+obj.name+".png"),
			HXP.getBitmap("gfx/"+obj.name+"_normal.png"))
		);
		e.graphic = g;
		e.setHitbox(g.width - 1, g.height - 1);
		return e;
	}
	
	static public function guard(obj:TmxObject):Entity
	{
		var e = new Guard(obj.x, obj.y);
		var g = new NormalMappedImage(
			new NormalMappedBitmapData(HXP.getBitmap("gfx/"+obj.name+".png"),
			HXP.getBitmap("gfx/"+obj.name+"_normal.png"))
		);
		e.graphic = g;
		e.relativeX = Std.parseInt(obj.custom.resolve("relX"));
		return(e);
	}
	
	static public function throne(obj:TmxObject):Entity
	{
		return(genericEntity(obj));
	}
	
	static public function crown(obj:TmxObject):Entity
	{
		return(genericEntity(obj));
	}
	
	static public function genericEntity(obj:TmxObject):Entity
	{
		var e = new Entity(obj.x, obj.y);
		var g = new NormalMappedImage(
			new NormalMappedBitmapData(HXP.getBitmap("gfx/"+obj.name+".png"),
			HXP.getBitmap("gfx/"+obj.name+"_normal.png"))
		);
		e.graphic = g;
		e.setHitbox(g.width, g.height);
		return e;
	}
	
	static public function block(obj:TmxObject) :Entity
	{
		var e = new Entity(obj.x, obj.y);
		e.graphic = new Image(HXP.getBitmap("gfx/block.png"));
		e.type = "solid";
		e.setHitbox(16, 64);
		return e;
	}
	
	static public function inivisibleBlock(obj:TmxObject) :Entity
	{
		var e = new Entity(obj.x, obj.y);
		e.type = "solid";
		e.setHitbox(16, 64);
		return e;
	}
	
	
}