package world;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Emitter;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.Panel;
import com.haxepunk.HXP;
import com.haxepunk.normalmap.graphic.NormalMappedImage;
import com.haxepunk.normalmap.Light;
import com.haxepunk.Scene;
import com.haxepunk.Sfx;
import com.haxepunk.tmx.TmxEntity;
import com.haxepunk.tmx.TmxObject;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import nme.Assets;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.geom.Vector3D;
import nme.text.TextField;
import nme.text.TextFieldAutoSize;
import nme.text.TextFormat;
import nme.text.TextFormatAlign;
import plateformer.ControlledHero;
import plateformer.Guard;

/**
 * ...
 * @author Samuel Bouchet
 */

class GameWorld extends Scene
{
	public static var instance:GameWorld ;
	public var lights:Array<Light>;
	
	private var ground:Entity;
	private var hero:ControlledHero ; 
	
	private var displayedText:TextField;
	var pressKeyLbl:Label;
	private var emitter:Emitter;
	
	var entities: Array<Entity>;
	private var backPanel:Panel;
	private var torch:Light;
	
	var nbLigne:Int = 1;
	static var pos:Vector3D = new Vector3D();
		
	private var equipSFX:Sfx;
	private var startMusic:Sfx;
	private var fightMusic:Sfx;
	private var banquetMusic:Sfx;
	private var takeSwordSfx:Sfx;
	private var hitDragonSfx:Sfx;
	private var displayedTextMask:Bitmap;

	public function new() 
	{
		super();
		
		lights = new Array<Light>();
		entities = new Array<Entity>();
		
		startMusic = new Sfx("music/start.mp3");
		fightMusic = new Sfx("music/fight.mp3");
		banquetMusic = new Sfx("music/banquet.mp3");
		equipSFX = new Sfx("sfx/equip.mp3");
		takeSwordSfx = new Sfx("sfx/takeSword.mp3");
		hitDragonSfx = new Sfx("sfx/hitDragon.mp3");
		
		instance = this;
	}


	override public function update() 
	{
		super.update();
		
		updateCamera();
		
		backPanel.update();
		pressKeyLbl.update();
		
		if (!Story.instance.occured("refightDragon")) {
			for (i in 0...3)
			{
				emitter.emit("water", 0, 0);
			}
		}
		
		updateLights();
		
		checkScenario();
		
		if (Input.pressed(Key.ESCAPE)) {
			HXP.scene = WelcomeWorld.instance;
		}
	}
	
	
	public function tell(text:String) 
	{
		displayedText.text += "\n" + text;
		var h = -displayedText.height + 67;
		var t = HXP.tween(displayedText, { y: h }, 60);
	}

	
	private function checkScenario() 
	{
		hero.collideInto("event", hero.x, hero.y, entities);
		var collided = false;
		for (e in entities) {
			trigger(e.name);
			if (Story.instance.occured("goOut")) {
				unlock("roomDoor");
			}
			if (Story.instance.occured("refightDragon")) {
				unlock("banquetDoor");
			}
			if (Story.instance.occured("enterHall")) {
				unlock("throneDoor");
			}
			collided = true;
		}
		if(!collided) {
			removePressKeyInstruction();
		}
		entities.splice(0, entities.length);
	}
	
	public function trigger(triggerName:String) 
	{
		switch (triggerName) 
		{
			case "stuff":
				if (Story.instance.occured("takeStuff") && !Story.instance.occured("goOut")) {
					if (Input.pressed("action1")) {
						playEvent("goOut");
					} else {
						displayPressKeyInstruction();
					}
				}
				if (Story.instance.occured("intro") && !Story.instance.occured("takeStuff")) {
					playEvent("takeStuff");
				}
			case "dragonSign":
				if (Story.instance.occured("goOut") && !Story.instance.occured("enterCave")) {
					playEvent("enterCave");
				}
			case "dragon":
				if (Story.instance.occured("takeSword") && !Story.instance.occured("refightDragon")) {
					if (Input.pressed("action1")) {
						playEvent("refightDragon");
					} else {
						displayPressKeyInstruction();
					}
				}
				if (Story.instance.occured("enterCave") && !Story.instance.occured("fightDragon")) {
					if (Input.pressed("action1")) {
						playEvent("fightDragon");
					} else {
						displayPressKeyInstruction();
					}
				}
			case "sword":
				if (Story.instance.occured("fightDragon") && !Story.instance.occured("takeSword")) {
					if (Input.pressed("action1")) {
						playEvent("takeSword");
					} else {
						displayPressKeyInstruction();
					}
				}
			case "banquetTable":
				if (Story.instance.occured("refightDragon") && !Story.instance.occured("enterHall")) {
					playEvent("enterHall");
				}
			case "guard":
				if (Story.instance.occured("enterHall") && !Story.instance.occured("enterThrone")) {
					playEvent("enterThrone");
				}
			case "throne":
				if (Story.instance.occured("sitThrone") && !Story.instance.occured("end")) {
					if (Input.pressed("action1")) {
						playEvent("end");
					} else {
						displayPressKeyInstruction();
					}
				}
				if (Story.instance.occured("enterThrone") && !Story.instance.occured("sitThrone")) {
					if (Input.pressed("action1")) {
						playEvent("sitThrone");
					} else {
						displayPressKeyInstruction();
					}
				}
			default:
				removePressKeyInstruction();
		}
	}
	
	
	private function displayPressKeyInstruction() 
	{
		if (pressKeyLbl.scene == null) {
			pressKeyLbl.localX = 240/2 - pressKeyLbl.width/2;
			pressKeyLbl.localY = 160 - pressKeyLbl.height-1;
			add(pressKeyLbl);
		}
	}
	
	private function removePressKeyInstruction() 
	{
		if (pressKeyLbl.scene != null) {
			remove(pressKeyLbl);
		}
	}
	
	private function playEvent(eventName:String) {
		switch (eventName) 
		{
			case "intro":
				startMusic.play();
				tell("Once was a Knight in a mourning Kingdom. The king just died, but people need hope.");
			case "takeStuff":
				tell("\"- Take this.\" says the guard, \"It's a special day for you today.\".\n\"- Yes, a Kingdom is waiting for me !\"");
			case "goOut":
				equipSFX.play();
				hero.equipStuff();
				var e = this.getInstance("stuff");
				if (e != null) remove(e);
				refreshHeroLighting();
				tell("He knew it would be hard, the dragon already defeated the old king. He has to be brave !");
			case "enterCave":
				startMusic.stop();
				fightMusic.play();
				tell("He could not come back. Gathering his courage, he engaged the monster : \"YAAAAAAAHHHHHHHH\".");
			case "fightDragon":
				hitDragonSfx.play();
				hero.breakStuff();
				refreshHeroLighting();
				tell("The dragon was tough and his blue flames melted the Knight' sword.\nHe had to find a new one.");
			case "takeSword":
				takeSwordSfx.play();
				hero.equipNewSword();
				var e = this.getInstance("sword");
				if (e != null) remove(e);
				refreshHeroLighting();
				tell("\"Here is the divine sword ! I'll get you with this, dragon !\"");
			case "refightDragon":
				hitDragonSfx.play();
				tell("The knight smashed the beast, again and again, until he could not breathe fire anymore.");
			case "enterHall":
				fightMusic.stop();
				banquetMusic.play();
				tell("When arriving to the castle hall, a banquet was waiting for him. \nHis mates were cheering their new valorous King ! Hurray !");
			case "enterThrone":
				hero.equipNormal();
				var e = this.getInstance("sword");
				if (e != null) remove(e);
				refreshHeroLighting();
				hero.canMoveLeft = false;
				var guards = new Array<Guard>();
				this.getClass(Guard, guards);
				for (g in guards) {
					g.hero = hero;
					g.followHero = true;
				}
				tell("The guards leaded him until the throne, were he sit to be crown.");
			case "sitThrone":
				// move up the hero
				hero.y -= 16;
				hero.x = getInstance("crown").x;
				hero.canMove = false;
				tell("Inside of him, he felt the most amazing warm satisfaction. He did it ! \nHe killed the dragon and saved everyone ! He had fulfiled his duty.");
			case "end":
				playEnd();
		}
		removePressKeyInstruction();
		Story.instance.nextEvent();
	}
	
	private function playEnd() 
	{
		// remove narrator
		if (HXP.stage.contains(displayedText)) {
			HXP.stage.removeChild(displayedText);
		}
		if (backPanel.scene != null) {
			backPanel.scene.remove(backPanel);
		}
		
		HXP.alarm(240, function (e):Void 
		{
			// set up the light
			var electricity = new Light(Std.int(hero.x + 8), Std.int(hero.y - 10), 0x8A7DFF, 400, 0.9, 0);
			lights.push(electricity);
			
			// play sound
			var chargeDefibrillator = new Sfx("sfx/defibrilator_charge.mp3");
			chargeDefibrillator.play();
			HXP.alarm(180, function (e):Void {
				// blackscreen at end of sound
				var blackScreen:Stamp = new Stamp(new BitmapData(240, 160, false, 0x000000));
				var e:Entity = new Entity();
				e.graphic = blackScreen;
				e.x = camera.x;
				e.y = camera.y;
				add(e);
				e.layer = 4;
				
				Label.defaultFont = Assets.getFont("font/lythgame.ttf");
				var theEnd:Label = new Label("Long live, the King !");
				theEnd.size = 8;
				theEnd.x = camera.x + 240 / 2 - theEnd.width / 2;
				theEnd.y = camera.y + 160 / 2 - theEnd.height / 2;
				theEnd.color = 0xFFD700;
				theEnd.layer = 3;
				theEnd.text = "Long live,";

				HXP.alarm(120, function (e):Void {
					add(theEnd); 
				});
				HXP.alarm(180, function (e):Void {
					theEnd.text += " the King !";
					HXP.alarm(180, function (e):Void {
						HXP.scene = WelcomeWorld.instance;
					});
				});
			});
		});
	}
	

	
	private function unlock(string:String) 
	{
		var e = this.getInstance(string);
		if (e!=null) {
			this.remove(e);
		}
	}
	
	override public function begin():Dynamic 
	{
		torch = new Light(100, 0, 0xFFFFFF, 40, 2);
		lights.push(torch);
		
		loadLevel("map/carte2.tmx", "gfx/tileset.png");
		
		initNarrator();
		
		Label.defaultFont = nme.Assets.getFont("font/lythgame.ttf");
		pressKeyLbl = new Label("Press 'e' to perform action");
		pressKeyLbl.color = 0xFFFFFF;
		pressKeyLbl.layer = 4;
		pressKeyLbl.size = 8;
		pressKeyLbl.followCamera = true;
		
		super.begin();
		
		playEvent("intro");
		
		//Testing
		//playEvent(	"takeStuff");
		//playEvent(	"goOut");
		//playEvent(	"enterCave");
		//playEvent(	"fightDragon");
		//playEvent(	"takeSword");
		//playEvent("refightDragon");
	}
	
	private function initNarrator() 
	{
		backPanel = new Panel(0, 0, 240, 19);
		backPanel.followCamera = true;
		backPanel.layer = 5;
		add(backPanel);
		
		displayedText = new TextField();
		
		Label.defaultFont = nme.Assets.getFont("font/lythgamescript.ttf");
		var format = new TextFormat(Label.defaultFont.fontName, 32, 0xFFFFFF);
		format.align = TextFormatAlign.LEFT;
		format.leading = 2;
		displayedText.defaultTextFormat = format;
		displayedText.y = 34;
		displayedText.x = 20;
		displayedText.autoSize = TextFieldAutoSize.LEFT;
		displayedText.selectable = false;
		displayedText.embedFonts = true;
		displayedText.setTextFormat(format);
		displayedText.textColor = 0x4F410F;
		
		displayedTextMask = new Bitmap(new BitmapData(922, 56, false, 0xFFFFFF));
		displayedTextMask.y = 15;
		displayedTextMask.x = 20;
		
		displayedText.mask = displayedTextMask;

		HXP.stage.addChild(displayedTextMask);
		HXP.stage.addChild(displayedText);
		HXP.stage.setChildIndex(displayedText,HXP.stage.numChildren-1);
		
	}
	
	private function loadLevel(map:String, tileset:String) 
	{
		var level:TmxEntity = new TmxEntity(map);
		level.loadGraphic(tileset, ["tiles"]);
		level.loadMask("tiles", "solid");
		
		var objects:Array<TmxObject> = level.map.getObjectGroup("objects").objects;
		for (obj in objects) {
			if(obj.name != null){
				var entity:Entity = null;
				switch (obj.name) 
				{
					case "hero":
						entity = EntityFactory.hero(obj);
						entity.layer = 20;
						hero = cast(entity, ControlledHero);
					case "stuff":
						entity = EntityFactory.stuff(obj);
						entity.layer = 19;
					case "firstGuard":
						entity = EntityFactory.firstGuard(obj);
						entity.layer = 21;
					case "dragonSign":
						entity = EntityFactory.dragonSign(obj);
						entity.layer = 21;
					case "dragon":
						entity = EntityFactory.dragon(obj);
						entity.layer = 21;
						createDragonEmitter(obj.x, obj.y);
					case "sword":
						entity = EntityFactory.sword(obj);
						entity.layer = 19;
					case "admirator":
						entity = EntityFactory.admirator(obj);
						entity.layer = 22;
					case "guard":
						entity = EntityFactory.guard(obj);
						var layer = obj.custom.resolve("layer");
						if (layer!=null) {
							entity.layer = Std.parseInt(layer);
						} else {
							entity.layer = 21;
						}
						
					case "throne":
						entity = EntityFactory.throne(obj);
						entity.layer = 21;
					case "crown":
						entity = EntityFactory.crown(obj);
						entity.layer = 21;
					default :
						if (obj.type == "block") {
							entity = EntityFactory.block(obj);
							
						} else if (obj.type == "invisibleBlock") {
							entity = EntityFactory.inivisibleBlock(obj);
							
						} else {
							entity = EntityFactory.genericEntity(obj);
						}
						entity.layer = 21;
				}
				if (entity != null) {
					entity.name = obj.name;
					add(entity);
					// event attached ?
					if (obj.type == "event") {
						entity.type = "event";
						entity.setHitbox(obj.width, obj.height);
					}
					// preload light
					if (entity.graphic != null && Std.is(entity.graphic, NormalMappedImage)) {
						cast(entity.graphic, NormalMappedImage).updateLightning(lights[0].position, lights);
					}
				}
			}
		}
		
		add(level);
	}
	
	private function createDragonEmitter(x:Float, y:Float) 
	{
		var bd:BitmapData = new BitmapData(8, 2, true, 0xffffffff);
		emitter = new Emitter(bd,2,2);
		emitter.relative = true;
		emitter.newType("water", [0,1,2,3]);
		emitter.setMotion("water", 190, 80, 120, 80, 40, 40);
		emitter.setColor("water", 0xA4E8F7, 0x0A1C45);
		emitter.setGravity("water", 9.81);
		emitter.setAlpha("water", 1, 1);
		
		var e:Entity = new Entity(x+27, 32);
		e.addGraphic(emitter);
		e.layer = 18;
		add(e);
		
	}
	
	private function updateLights():Void 
	{
		// update light position
		if (lights.length > 0) {
			if (Input.mouseDown) {
				lights[0].position.setTo(Input.mouseX+camera.x, Input.mouseY+camera.y, 0);
			}
			if (Input.mouseUp) {
				lights[0].position.setTo(-1000, -1000,0);
			}
		}
		
		// udpate lightning
		var e:Entity, 
			fe:FriendEntity = _updateFirst;
		while (fe != null)
		{
			e = cast(fe, Entity);
			if (e.graphic != null && Std.is(e.graphic, NormalMappedImage) && e.x < camera.x + 240 && e.x + e.width > camera.x) {
				pos.x = e.x ;
				pos.y = e.y;
				pos.z = 0;
				cast(e.graphic, NormalMappedImage).updateLightning(pos, lights);
			}
			fe = fe._updateNext;
		}
	}
	
	private function updateCamera():Void 
	{
		camera.x = hero.x - 120 ;
	}
	
	private function refreshHeroLighting():Void 
	{
		pos.x = hero.x;
		pos.y = hero.y;
		pos.z = 0;
		cast(hero.graphic, NormalMappedImage).updateLightning(lights[0].position, lights);
	}
	
	override public function end():Dynamic 
	{
		startMusic.stop();
		fightMusic.stop();
		banquetMusic.stop();
		 
		removeAll();
		if (HXP.stage.contains(displayedText)) {
			HXP.stage.removeChild(displayedText);
		}
		if (HXP.stage.contains(displayedTextMask)) {
			HXP.stage.removeChild(displayedTextMask);
		}
		super.end();
	}
	
}