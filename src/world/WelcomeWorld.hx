package world;
import com.haxepunk.animation.AnimatedSprite;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.gui.Label;
import com.haxepunk.HXP;
import com.haxepunk.normalmap.Light;
import com.haxepunk.Scene;
import com.haxepunk.gui.Button;
import com.haxepunk.Sfx;

/**
 * ...
 * @author Samuel Bouchet
 */

class WelcomeWorld extends Scene
{
	private var bJouer:Button;
	private var title:Entity;
	private var credit:Label;
	private var introMusic:Sfx;
	
	public static var instance:WelcomeWorld ;

	public function new()
	{
		super();
		
		Label.defaultFont = nme.Assets.getFont("font/lythgamescript.ttf");
		bJouer = new Button("Play >");
		bJouer.x = 190;
		bJouer.y = 40 ;
		bJouer.size = 16;
		bJouer.addEventListener(Button.CLICKED, function(e:ControlEvent) {
			new Story();
			HXP.scene = new GameWorld();
		});
		bJouer.layer = 10;
		
		title = new Entity(0, 0, new Stamp("gfx/title.png"));
		title.layer = 20;
		
		introMusic = new Sfx("music/intro.mp3");
		
		instance = this;
	}
	
	override public function begin():Dynamic
	{
		introMusic.play(1);
		
		add(bJouer);
		add(title);
		
		super.begin();
	}
	
	override public function end():Dynamic
	{
		introMusic.stop();
		removeAll();
		super.end();
	}
	
}