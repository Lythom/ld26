package ;

/**
 * ...
 * @author Samuel Bouchet
 */
class Story
{
	
	public static var instance:Story;
	
	private var currentEventIndex:Int = 0;
	public var currentEvent:String;
	
	public var eventList:Array<String>;
	public var occuredEvent:Hash<Bool>;
	
	public function new() 
	{
		instance = this;
		
		currentEventIndex = 0;
		eventList = [
			"intro",
			"takeStuff",
			"goOut",
			"enterCave",
			"fightDragon",
			"takeSword",
			"refightDragon",
			"enterHall",
			"enterThrone",
			"sitThrone",
			"end"
		];
		occuredEvent = new Hash<Bool>();
		for (e in eventList){
			occuredEvent.set(e, false);
		}
		currentEvent = eventList[currentEventIndex];
	}
	
	public function occured(event:String):Bool {
		return occuredEvent.get(event);
	}
	
	public function isOver() {
		return currentEventIndex > eventList.length;
	}
	
	public function nextEvent():String {
		occuredEvent.set(currentEvent, true);
		
		currentEventIndex++;
		if (currentEventIndex < eventList.length) {
			currentEvent = eventList[currentEventIndex];
		}
		return currentEvent;
	}
	
	
	
}