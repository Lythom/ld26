package com.haxepunk.normalmap.graphic;
import com.haxepunk.graphics.Image;
import com.haxepunk.normalmap.NormalMappedBitmapData;
import nme.geom.Rectangle;
import nme.geom.Vector3D;

/**
 * ...
 * @author Samuel Bouchet
 */

class NormalMappedImage extends Image
{

	public function new(source:NormalMappedBitmapData, clipRect:Rectangle = null, name:String = "")
	{
		super(source, clipRect, name);
	}
	
	public var normalMapSource(getNormalMapSource, null):NormalMappedBitmapData;
	private function getNormalMapSource():NormalMappedBitmapData { return cast(_source); }
	
	public function updateLightning(bitmapPosition:Vector3D, lights:Array<Light>) 
	{
		normalMapSource.updateLightning(bitmapPosition, lights);
		updateBuffer();
	}
	
	
}